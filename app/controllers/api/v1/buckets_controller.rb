module Api
  module V1
    class BucketsController < ApiController
      def index
        respond_with Bucket.all
      end
    end
  end
end
