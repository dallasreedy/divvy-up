class Bucket < ActiveRecord::Base
  default_scope order(:position)

  attr_accessible :name, :position, :percentage

  validates :name, :position, :percentage, presence: true
  validates :name, :position, uniqueness: true

  after_initialize :initialize_position, if: :new_record?

  class << self
    def reorder_rows_for_move(from, to)
      sql_col = connection.quote_column_name :position

      if from - to < 0 # down (e.g. from 2 to 5)
        where("#{sql_col} > ? AND #{sql_col} <= ?", from, to).update_all "#{sql_col}=#{sql_col}-1"
      else # up (e.g. from 9 to 3)
        where("#{sql_col} >= ? AND #{sql_col} < ?", to, from).update_all "#{sql_col}=#{sql_col}+1"
      end
    end
  end

  def initialize_position
    max_position = self.class.maximum(:position)

    if read_attribute(:position).blank? && max_position
      write_attribute :position, max_position + 1
    end
  end

  def move_to(position)
    return unless position.to_i.to_s == position.to_s

    position     = position.to_i
    old_position = self.position

    return if old_position == position

    self.class.reorder_rows_for_move old_position, position
    self.update_attribute :position, position
  end
end
