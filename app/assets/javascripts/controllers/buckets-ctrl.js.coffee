@BucketsCtrl = ($scope, $filter, Buckets) ->
  $scope.buckets  = Buckets.query()
  $scope.amount   = 0

  $scope.$watch '_amount', (newValue, oldValue) ->
    $scope.amount = window.accounting.unformat(newValue)

  $scope.divvyAmount = (bucket) ->
    bucket.divvyAmount = previousAmount(bucket.order) * bucket.percentage

  $scope.totalAfterDivvying = (bucket) ->
    bucket.totalAfterDivvying = previousAmount(bucket.order) - bucket.divvyAmount

  previousAmount = (orderIndex) ->
    previousBucket(orderIndex).totalAfterDivvying or $scope.amount

  previousBucket = (orderIndex) ->
    filtered = $filter('filter')($scope.buckets, { 'order': orderIndex - 1 })
    filtered[0] || {}

@BucketsCtrl.$inject = ['$scope', '$filter', 'Buckets']
