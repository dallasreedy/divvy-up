@app.factory 'Buckets', ['$resource', ($resource) ->
  $resource '/api/v1/buckets/:bucketId'
]
