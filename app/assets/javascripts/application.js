//= require 'foundation'
//= require 'accounting'
//= require 'angular'
//= require 'angular-resource'
//= require 'divvy-up'
//= require_tree ./resources
//= require_tree ./controllers

$(document).foundation();
