DivvyUp::Application.routes.draw do
  scope module: 'api', path: 'api', format: 'json' do
    scope module: 'v1', path: 'v1' do
      resources :buckets, only: :index
    end
  end

  root to: 'application#index'
end
