Bucket.create! [
  { name: 'Family',   percentage: 0.05, position: 2 },
  { name: 'Puppy',    percentage: 0.01, position: 4 },
  { name: 'Savings',  percentage: 0.10, position: 1 },
  { name: 'Tithe',    percentage: 0.10, position: 0 },
  { name: 'Travel',   percentage: 0.05, position: 3 }
]
