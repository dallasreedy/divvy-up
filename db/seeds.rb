# Seeds for all go here:

# Environment-specific seeds:
seed_file = Rails.root.join('db', 'seeds', "#{Rails.env}_seeds.rb")
require seed_file if File.exists?(seed_file)
