### Iterations

0. ✓ Set up the app!
  a. ✓ HAML & SASS
  b. ✓ CoffeeScript everything
  c. ✓ AngularJS front-end
  d. ✓ create JSON API end points (version 1)
  e. ✓ use MongoDB? (is that free on Heroku? free option) worry about this later!
  f. ✓ use Redis? (is that free on Heroku? free option) worry about this later!
  g. ✓ deploy to Heroku
  h. ✓ purchase and set up domain (divvy-up.us)
    * ✓ purchase through NameCheap, set up DNS name servers to point to Dreamhost
    * ✓ wait for it to show up in Dreamhost
    * ✓ set up divvy-up.us to redirect to www.divvy-up.us
    * ✓ set the DNS on Dreamhost for the www CNAME to point to Heroku app
    * ✓ need to add domain to Heroku
1. ✓ Add Bucket model and retrieve buckets from there
  a. ✓ Bucket model
  b. ✓ development-only data seed file with current buckets
  c. ✓ replace existing hard-coded API call
  d. ✓ test the Bucket model :)
2. Integrate a CSS structure library (ZURB Foundation)
  a. ✓ learn it!
  b. ✓ add it!
  c. - implement it!
3. Make offline (guest) accounts
  a. figure out data storage solution
    * start with just AngularJS
    * try adding in local storage support
  a. can get to the divvying up page, can add buckets and whatnot
  b. nothing really gets saved
    * use local storage or something like redis?
  c. if using a guest account, add marketing type message to create an account (benefits)
4. Add User model which owns the buckets
  a. User model
  b. has_many :buckets
  c. get buckets through the user (always)
  d. simple sign-up and login using email and password
5. Add Rails Admin for backend admin stuff (and to get Devise and CanCan)
  a. add rails_admin gem
  b. configure for my personal set up
  c. replace basic email/password with Devise
6. Social site login
  a. Twitter OAuth
  b. Facebook OAuth
  c. Google OAuth?
  d. other?
  e. link with existing user accounts
  f. link multiple services to one user account
7. Marketing page
  a. polish idea with the paycheck coming in and getting split into buckets
  b. make note that even distribution is default, but linear is available
  c. call to action is simply to try it out (for free, no log in required)
8. Add tracking & stats
  a. get a Google Analytics account
  b. add exception notifiers
  c. consider something like New Relic for overall app health tracking
  d. other?
9. Publicity
  a. give to all my family, friends and coworkers
    * get feedback?
  b. post to Twitter and Facebook
  c. research what else to do--Hacker News? Slash-dot? Financial services (banks, Mint.com, etc.)?
  d. hope it goes viral
