require 'test_helper'

class BucketTest < ActiveSupport::TestCase
  describe 'a new Bucket' do
    subject { Bucket.new }

    it 'must have a name' do
      subject.wont_be :valid?
      subject.errors[:name].wont_be_empty
    end

    it 'must have a unique name' do
      subject.name = buckets(:one).name
      subject.wont_be :valid?
      subject.errors[:name].wont_be_empty
    end

    it 'will receive the next position by default' do
      subject.position.must_equal Bucket.last.position + 1
    end

    it 'must have a position' do
      subject.position = nil
      subject.wont_be :valid?
      subject.errors[:position].wont_be_empty
    end

    it 'must have a unique position' do
      subject.position = buckets(:one).position
      subject.wont_be :valid?
      subject.errors[:position].wont_be_empty
    end

    it 'must have a percentage' do
      subject.wont_be :valid?
      subject.errors[:percentage].wont_be_empty
    end

    it 'is valid if it has a unique name, unique position and a percentage' do
      subject.name = 'Unique Name'
      # subject.position is auto-set to the next value
      subject.percentage = 0.1 # 10%
      subject.must_be :valid?
    end
  end

  describe 'an existing Bucket' do
    subject { buckets(:two) }

    describe 'repositioning' do
      def check_order(*expected)
        Bucket.all.map {|b| "#{b.position}. #{b.name}"}.must_equal expected
      end

      before {
        check_order '0. Bucket1', '1. Bucket2', '2. Bucket3', '3. Bucket4', '4. Bucket5'
      }

      it 'can be moved down the list' do
        subject.move_to 4
        check_order '0. Bucket1', '1. Bucket3', '2. Bucket4', '3. Bucket5', '4. Bucket2'
      end

      it 'can be moved up the list' do
        subject.move_to 0
        check_order '0. Bucket2', '1. Bucket1', '2. Bucket3', '3. Bucket4', '4. Bucket5'
      end
    end
  end
end
