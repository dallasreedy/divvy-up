ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"

# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
# require "minitest/rails/capybara"

# Uncomment for awesome colorful output
require "minitest/pride"

Turn.config.format = :pretty

module MiniTest::Expectations
  infect_an_assertion :assert_redirect_to,  :must_redirect_to
  infect_an_assertion :assert_template,     :must_render_template
  infect_an_assertion :assert_response,     :must_respond_with
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  class << self
    alias :context :describe
  end
end
